from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import requests
import json
from bs4 import BeautifulSoup
from slack_webhook import Slack
from airflow.models import Variable

args = {
        'owner': 'airflow',
        'depends_on_past': False,
        'start_date': datetime(2020, 6, 22),
        'email': ['airflow@localhost'],
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5),
        }

dag = DAG(
        'lego_checker',
        default_args=args,
        schedule_interval='*/15 * * * *',
        catchup=True,
        )

def scrape_sets():
    sets_of_interest = Variable.get("lego_sets").split(", ")
    print("Checking the status of {sets}".format(sets=sets_of_interest))
    lego_url = 'https://www.lego.com/en-us/product/'
    product_dict = {}

    for product in sets_of_interest:
        product = product.strip("'")
        print("Retrieving results for {set}".format(set=lego_url+product))
        page = requests.get(lego_url + product)
        parsed_page = BeautifulSoup(page.content, 'html.parser')

        for item in parsed_page.find_all('p', attrs={'data-test' : True}):
            if item['data-test'] == 'product-overview-availability':
                product_dict[product] = item.getText()
                print("{set} is {availability}".format(set=product, availability=item.getText()))

    with open('/home/airflow/airflow/dags/sets.json', 'w') as file:
        file.write(json.dumps(product_dict))

    return(product_dict)


def ping_slack(item, availability):
    slack = Slack(url='https://hooks.slack.com/services/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')
    lego_url = 'https://www.lego.com/en-us/product/'
    slack.post(text="{set} is {availability}. To purchase, navigate to {url}".format(set=item, availability=availability, url=lego_url+item))
    return True


def check_sets(**kwargs):
    print("The sets to process are {sets}".format(sets=Variable.get("lego_sets")))

    with open('/home/airflow/airflow/dags/sets.json') as read_file:
        existing_state = json.load(read_file)

    new_state = scrape_sets()

    for key, value in new_state.items():
        if key in existing_state:
            if value != existing_state.get(key) and (value == 'Available now' or value.find('Backorders') != -1):
                ping_slack(key, value)
        elif key not in existing_state and (value == 'Available now' or value.find('Backorders') != -1):
            ping_slack(key, value)

    return True


start_task = DummyOperator(
    task_id="start",
    retries=3,
    dag=dag
)


check_lego_task = PythonOperator(
    task_id="check_sets",
    provide_context=True,
    python_callable=check_sets,
    retries=3,
    dag=dag
)


end_task = DummyOperator(
    task_id="end",
    retries=3,
    dag=dag
)

start_task >> check_lego_task >> end_task
